# Springschool_2022_Datenvisualisierung_und_Analyse_mit_R

Materialien zum Workshop "Datenvisualisierung (und Analyse) mit R" 

### Mo (Theorietag)

__10:15-11:45 SESSION 1__

    90 min Kurzeinführung Datenvisualisierung (Wisiorek, Englmeier)

__*MITTAGSPAUSE (12:00-13:00)*__

__13:15-14:45 SESSION 2__

    90 min Techniken + Libraries (Grundlagen): 
        45 min: Gängige Techniken I (Englmeier)
        45 min: Visualisierung Praxis I (Wisiorek, Englmeier)

__*PAUSE (15:00-15:15)*__

 __15:15-16:45 SESSION 3__ (nach PDF-Zeitplan; Website: 15:30-17:00)

    90 min Techniken + Libraries (Anwendungen): 
        45 min: Graphenvisualisierung (Englmeier)
        45 min: Clustervisualisierung (Wisiorek)



### Di (Praxistag)

__09:15-10:45 SESSION 1__ (nach PDF-Zeitplan; Website: 09:30-11:00)

    45 min Gruppenbildung & Themenentwicklung/-wahl
    45 min Erarbeitung geeigneter Visualisierungen

__11:15-12:45 SESSION 2__ (nach PDF-Zeitplan; Website: 13:15-14:45)

    90 min Erarbeitung geeigneter Visualisierungen

__*MITTAGSPAUSE (13:00-14:00)*__

__14:15-15:45 SESSION 3__ (nach PDF-Zeitplan; Website: 15:30-17:00)

    45 min Erarbeitung geeigneter Visualisierungen 
    45 min Auswertung und Präsentation 
