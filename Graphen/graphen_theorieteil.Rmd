---
title: "Gängige Techniken der Daten- & Informationsvisualisierung: Graphen"  
author: "Dr. Tobias Englmeier"
date: "`r Sys.Date()`"
output:
  powerpoint_presentation: default
  slidy_presentation: default
  beamer_presentation: default
  ioslides_presentation: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::include_graphics('./img/bg_test.png')

library(igraph)   
library(igraph)
library(graphlayouts)
```

## Was sind Graphen eigentlich?

-   Mathematisch ist ein Graph $G$ ein geordnetes Paar aus Knoten (engl. Nodes oder Vertices) $V$ und Kanten (engl. Edges) $E$.

-   Die Kanten eines Graphen können entweder *gerichtet* oder *ungerichtet* sein. Gerichtet Kanten werden als Pfeil dargestellt.

-   Zudem können die Kanten eines Graphen eine Beschriftung (engl. Label) haben. Ein Label $\alpha$ stellt eine beliebige Zeichenkette dar und lässt sich formal als Tupel $(v,\alpha,v')$ schreiben, wenn $v,v'\in V$.

## Ein einfaches Beispiel

![Ein einfacher gerichteter Graph mit drei Knoten und drei Kanten](img/bsp_graph1.png "Ein einfacher gerichteter Graph mit drei Knoten und drei Kanten"){width="1024"}

## Warum ist es sinnvoll Daten als Graphen darzustellen?\*

-   Beziehungen zwischen Daten können besser erfasst werden

-   Dadurch können komplexe Systeme und deren inherente Mechaniken erkannt werden

-   Graphen- oder Netzwerkvisualisierungen können auch interaktiv genutzt werden, um Phänomene innerhalb großer Datensätze aufzudecken

\*nur einige Gründe

## Beispiel 1: Personennetzwerke

### Eigenschaften der Visualisierung:

-   Autoren bilden die Knoten
-   Kanten geben an, wie viele gemeinsame Textstellen existieren
-   Zusätzlich wird das Geburtsdatum jedes Autors als Farbattribut in jedem Knoten dargestellt

### Use Case:

-   Visualisierung hilft historische Verknüpfungen zwischen Autoren nachzuvollziehen
-   Hilft Cluster aber auch Inselphänome zu erkennen und trägt zu Erklärung dieser bei
-   Bietet den Ausgangspunkt für weitere Analysen des dargestellten Netzwerks

![Beziehungen zwischen Autoren verschiedener Epochen anhand gemeinsamer Textnutzung](img/bsp_graph2.png "Beziehungen zwischen Autoren verschiedener Epochen anhand gemeinsamer Textnutzung")

## Beispiel 2: Präfixgraph

### Eigenschaften der Visualisierung:

-   Präfixe bilden Knoten des Wortes (Cocoa)
-   Schwarze (solid und gestrichelte) Kanten bilden Erweiterungen um jeweils einen Buchstaben
-   Rote Kanten zeigen Suffixbeziehungen zwischen den Knoten
-   Kanten sind gerichtet

### Use Case:

-   Graph kann benutzt werden, um Indexstruktur besser zu verstehen bzw. deren Eigenschaften nachvollziehen zu können.
-   Visualisierung hift bei Überprüfung bzw. dem Finden von Fehlern des unterliegenden Algorithmus helfen.

![DAWG des Wortes "Cocoa"](img/dawg_with_suffixlinks.png "DAWG des Wortes 'Cocoa'")



## Beispiel 3: Multimodale Netzwerke

### Eigenschaften der Visualisierung:

-   Personen werden durch rot eingefärbte Knoten dargestellt, Gegenstände durch blaue; Anzahl der Erwähnungen bedingt Größe der Knoten

-   Kanten zeigen Beziehungen zwischen Personen und Gegenständen an, Label gibt Relationstyp an ("Tochter von", "besitzt", "verkauft" etc.)

-   Visualisierung ist interaktiv $\rightarrow$: Personen können ausgewählt werden und Daten nach bestimmten Attributen gefiltert werden

-   Einzelne Punkte können angeklickt werden, um Detailinformationen zu erhalten

### Use Case:

- Historische Netzwerkanalye 

- Erkenntnisse über Geld- und Kunstflüsse des Danziger Bürgertums des 16. Jht. 

![Interaktives Multimodales Netzwerk](img/geldkunstnetz.png "Interaktives Multimodales Netzwerk")

## Beispiel 4: Georeferenzierte Netzwerke

### Eigenschaften der Visualisierung:

-   Orte bilden Knoten, Farbe gibt das Herkunfstland an

-   Kanten zeigen Migrationsbewegungen von Personen

-   Visualisierung ist interaktiv $\rightarrow$ Daten können chronologisch gefiltert werden

-   Einzelne Punkte können angeklickt werden, um Detailinformationen zu erhalten

### Use Case:

-   Interaktive Darstellung trägt dazu bei, unterschiedliche Datenzustände zu erkennen und so schnell und unkompliziert Rückschlüsse über Migrationsbewegungen ziehen zu können.

-   Folgt dem "Mantra of Visual Information Seeking" : *Overview first, zoom and filter, then details-on-demand* von Ben Shneiderman

![Interaktive georeferenzierte Graphendarstellung](img/ths.png "Interaktive georeferenzierte Graphendarstellung")

## Mögliche Probleme bei der Graphenvisualisierung

1.  **Tools zur Graphenvisualisierung nutzen bestimmte Algorithmen, um etwa das Layout des Graphen zu bestimmen** $\rightarrow$ Manche Layouts sind nicht für große Datenmengen geeignet!

2.  **Wer die Kontrolle über die Daten hat, ist in der Lage Probleme besser zu lösen** $\rightarrow$ Ohne die Möglichkeit Daten zu filtern umzuformatieren oder gar bestimmt Teile ganz zu entfernen lassen sich Probleme bei der Darstellung oft nicht lösen!

3.  **Auch ein gutes Wissen über die verwendete Software (in diesem Falle igraph und ggraph) ist nötig, um evtl. Darstellungsprobleme in den Griff zu bekommen:** $\rightarrow$ Gestalterische Mittel, die aufgrund von Attributen ausgewählt werden, helfen dabei etwa wichtige Bereiche hervorzugeben und unwichtige zu verstecken.

![](img/bsp_graph3.png "Graph mit unleserlichem Inhalt")
