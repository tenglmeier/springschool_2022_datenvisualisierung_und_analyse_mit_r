---
title: 'Graphenvisualisierung igraph und ggraph'
author: "Dr. Tobias Englmeier"
date: "`r Sys.Date()`"
output: 
  html_document: 
    toc: yes
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
windows.options(antialias = "cleartype")
```

In R stehen mehrere Pakete zur Erzeugung und Darstellung von Graphen zur Verfügung. Zunächst werden nun die Basispakete **igraph**, **ggraph**, **graphlayouts** und **tidygraph** installiert:

## Installation

Zur Installation der Pakete aus dem CRAN Repositorium müssen folgende Befehl eingegeben werden:

```{r eval=FALSE, message=FALSE, warning=FALSE, include=FALSE, paged.print=FALSE}
install.packages("igraph")
install.packages("ggraph")
install.packages("graphlayouts") 
install.packages("tidygraph")

```

Nach der Installation werde die Pakete wie üblich geladen:

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library(igraph)   
library(ggraph)
library(graphlayouts)
library(tidygraph)
```

Um die erfolgreiche Installation zu überprüfen, kann ein vordefinierter Beispielgraph mit Hilfe der Funktion `make_graph()` erzeugt werden:

```{r}
g1 <- make_graph("Bull") # Frucht, House, Octahedron
plot(g1)
```

## Erstellen eines eigenen Graphen

Zur Erzeugung eigener Graphen mit *igraph* wird die Funktion `graph()` verwendet, die mit folgenden Argumenten aufgerufen werden kann:

1.  Einen Vector `edges`, der angibt, zwischen welchen Knoten Kanten verlaufen: Dabei zeigt jedes n-te Element auf jedes n+1-te Element:

-   Enthält der Vector numerische Werte, werden diese als Knoten-Ids verwendet
-   Enthält der Vector Zeichenketten, so bestimmen diese die Knotennamen.

2.  Eine natürliche Zahl `n`, die angibt, wieviele Knoten der Graph enthalten soll. Dieser Wert wird allerdings ignoriert, wenn es im Kanten-Vector eine größere Zahl gibt, als den Wert von `n`.

3.  Der boolsche Wert `directed` gibt an, ob der Graph gerichtet ist oder nicht.

Zur Erzeugung eines einfachen gerichteten Beispielgraphen kann also folgendes geschrieben werden:

```{r}
g1 <- graph(edges=c(1,2, 2,3, 3,4, 4,1), n=4, directed=T) 
plot(g1)
```

Werden anstelle von Knoten-Ids Knoten-Namen verwendet, wird der Parameter `n` nicht mehr gebraucht:

```{r}
g3 <- graph( c("Anton","Berta","Berta","Paul","Paul","Ida"))
plot(g3)
```

## Einlesen von Daten

Da die Bestandteile eines Graphen meist nicht per Hand eingegeben werden, existieren Methoden, die anhand extern geladener Daten *igraph* Netzwerke erstellen:

Zum Einlesen von Netzwerkdaten werden oft *csv*-Dateien verwendet. Dazu werden meist 2 Dateien benötigt; Eine, die alle Knoten enthält und eine, die die Verbindungen bzw. Kanten beinhaltet.

Wir lesen nun zwei Dateien eines Datensatzes ein, die Daten darüber enthalten, wie viele Fußballspieler verschiedener Nationen in den Ligen anderer Länder auflaufen. Für mehr Informationen zu diesem Datensatz [siehe hier](http://vlado.fmf.uni-lj.si/pub/networks/data/sport/football.htm).

```{r message=FALSE, warning=FALSE}
library(readr) # wird zum Einlesen von csv Dateien benötigt
nodes<-read_csv("data/football_nodes.csv")
edges<-read_csv("data/football_edges.csv")
```

Zum Anzeigen der ersten 10 eingelesenen Datensätze kann der Befehl `head()` benutzt werden.

```{r}
head(nodes,10)
head(edges,10)
```

## Darstellung mit *ggraph*

Zuerst wird mittels der Funktion `graph_from_data_frame()` ein igraph-Objekt erzeugt. Dieses wird im Beispiel in der Variablen `football_graph` gespeichert.

### Zeichen des Graphen

```{r}

football_graph= graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)

ggraph(football_graph) +
      # Kanten
      geom_edge_link() +
      # Knoten
      geom_node_point() 
```

### Ändern des Layouts

Der dargestellte Graph enthält Knoten und Kanten, deren Anordnung sich nach einem zufälligen Layout - Algorithmus richtet. Um das Layout der Knoten und Kanten zu beeinflussen wird dem `ggraph` ein weiterer Parameter hinzugefügt:

```{r}

football_graph= graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes) #
ggraph(football_graph, layout="star") + #"stress", "backbone", "kk"...
      # Kanten
      geom_edge_link() +
      # Knoten
      geom_node_point()
     
  
```

Wird etwa als Wert des neuen Paramters "star" ausgewählt, entsteht für den eingelesen Datensatz ein kreisförmige Anordung der Knoten. Weitere Layouts, die das Graphlayouts-Package unterstützt sind etwa "stress", "kk", "eigen" oder "backbone". Eine Liste der verfügbaren Layouts kann [der Dokumentation des Pakets](https://cran.r-project.org/web/packages/graphlayouts/graphlayouts.pdf) entnommen werden.

### Hinzufügen von Knoten - Labels

Die Namen der jeweiligen Knoten im Beispieldatensatz repäsentieren die 32 Ländern, die Fußballspieler transferieren. Damit erkenntlich wird, welcher Knoten welches Land repräsentiert, wird die Funktion `geom_node_label` hinzugefügt, in deren Parameter das im Label anzuzeigende Attribut sowie die Schriftart, Transparenz und der Wert für "repel" auf "True" gesetzt wird.

```{r}
football_graph= graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)
ggraph(football_graph, layout="star") +
      # Kanten
      geom_edge_link() +
      # Knoten
      geom_node_point() + 
      geom_node_label(aes(label = name),family="serif", alpha=0.9,repel=TRUE)
```

### Erstellen eines Themes

Um weitere Anpassungen vorzunehmen, kann wie auch bei ggplot2 ein **theme** erstellt werden, das bestimmte Parameter zur Gestaltung der Ausgabe enthält. Etwa lässt sich die Hintergrundfarbe ändern, Abstände (engl. Margins) hinzufügen und weitere Einstellungen vornehmen.

```{r message=FALSE, warning=FALSE, error=FALSE}

bgcolor="#F5F5F5" # "green3


my_theme<- function() {
  theme(
    text=element_text(family="Helvetica",face="bold"),
    plot.background = element_rect(fill=bgcolor),
    plot.margin = unit(c(10,10,10,10), units="mm"),
    axis.text = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.background = element_blank()
  )
}
```

Wie zuvor wir nun das neue Theme der Visualisierung hinzugefügt:

```{r message=FALSE, warning=FALSE, error=FALSE}
football_graph= graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)
ggraph(football_graph, layout="star") +
      # Kanten
      geom_edge_link() +
      # Knoten
      geom_node_point() + 
      geom_node_label(aes(label = name), alpha=0.9,repel="True") +
      my_theme()

```

### Darstellung von Knoten und Kanten - Attributen

Um die Anzahl der jeweils transferierten Spieler darzustellen, wird nun die Dicke der Kanten aufgrund des Kanten - Attributs "weight" gesetzt. Hierzu wird die Funktion `aes()` innerhalb von `geom_edge_link()` eingesetzt.

```{r message=FALSE, warning=FALSE, error=FALSE}
football_graph= graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)
ggraph(football_graph, layout="star") +
      # Kanten
      geom_edge_link(aes(alpha=weight,width=weight,start_cap = label_rect(node1.name), end_cap = label_rect(node2.name)), 
                   arrow = arrow(type = "closed", length = unit(2, 'mm')),colour="grey10") +
      scale_edge_width(range = c(0.2, 2.0)) +
      # Knoten
      geom_node_point(aes(size=auths)) +
      scale_size(range = c(2,10)) +
      geom_node_label(aes(label = name),size = 4, alpha=0.9,repel=TRUE) +
      coord_equal() +
      my_theme()
```

Gleichzeitig wird die Größe der Knoten nun auf den "Authorities" - Wert gesetzt. Was bedeutet, je größer ein Knoten, desto besser ist die Fußball-Liga des jeweiligen Landes. Damit das zirkulare Layout nicht verzerrt wird, kann zudem noch `coord_equal()` beigefügt werden.

### Label - Überlappung mit Knoten verhindern

Da die Knotenlabels nun oft mit den eigentlichen Knoten überlappen, kann die Position jedes Knotens dazu benutzt werden, um die Labels gleichmäßig zu anhand dieser zu verteilen. Dazu muss allerdings die `geom_node_label` Funktion ausgelagert werden, da zunächst die Knoten Positionen bekannt sein müssen.

Hierzu wird nun eine Variable `plt` erstellt, die den mit *ggraph* erstellten Plot enthält und die Funktion `geom_node_label()` dieser in einem zweiten Schritt hinzugefügt werden. Zudem kann anstelle von `geom_edge_link` nun `geom_edge_arc` benutzt werden.

```{r message=FALSE, warning=FALSE, error=FALSE}

football_graph = graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)
plt <- ggraph(football_graph, layout = 'linear', circular = TRUE) +
      # Kanten
      geom_edge_arc(aes(alpha=weight,width=weight,start_cap = label_rect(node1.name), end_cap = label_rect(node2.name)), 
      arrow = arrow(type = "closed", length = unit(2, 'mm'))) +
      scale_edge_width(range = c(0.2, 2.0)) +
      # Knoten
      geom_node_point(aes(size=auths)) +
      scale_size(range = c(2,10)) +
      coord_equal()+
      my_theme()
      

# Knoten-Labels
plt +
  geom_node_label(aes(label = name),family="serif", alpha=0.9,repel="False",nudge_x = plt$data$x * .1, nudge_y = plt$data$y * .1)

```

### Einfärbung der Knoten und Kanten anhand von Attributen

Einen noch besseren Überblick über die Daten erhält man, wenn die Knoten und Kanten nun anhand ihrer Nationalität eingefärbt werden. Hierzu werden die beiden `color` Parameter der jeweils mit den Attribute `name` der Knoten und `from` der Kanten besetzt. Da es sich bei beiden Attributen um diskrete Werte handelt, empfiehlt es sich, die `factor()` Funktion zu nutzen.

```{r message=FALSE, warning=FALSE, error=FALSE}

football_graph = graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)

test <- factor(V(football_graph)$name)
levels(test)
#cols_f(vcount(football_graph))
plt <- ggraph(football_graph, layout = 'linear', circular = TRUE) +
      # Kanten
      geom_edge_arc(aes(alpha=weight,color=factor(from),width=weight,start_cap = label_rect(node1.name), end_cap = label_rect(node2.name)), 
      arrow = arrow(type = "closed", length = unit(2, 'mm'))) +
      scale_edge_width(range = c(0.2, 2.0)) +
      # Knoten
      geom_node_point(aes(size=auths,color=factor(name))) +
      scale_size(range = c(2,10)) +
      coord_equal()+
      my_theme() +
      theme(legend.position = "none")

# Knoten-Labels
plt +
  geom_node_label(aes(label = name),family="serif", alpha=0.9,repel="False",nudge_x = plt$data$x * .15, nudge_y = plt$data$y * .15)

```

Es lässt sich nun gut erkennen, dass die Premier-League (GBR), die Primara Division (ESP) und Bundesliga (DEU) viele Spieler importieren, während viele andere Länder eher als "Hubs", also als Exporteure agieren. Starke Transferströme existieren zum Beispiel von Norwegen nach Großbritannien oder von Argentinien nach Italien.

Zur Darstellung der verschiedenen Knotentypen (in diesem Fall:`Hubs`,`Authorities`,`Hubs und Authorities`,`Andere`) können etwa auch verschiedene Symbole benutzt werden. Ebenfalls lässt sich, um besser erkennen zu können, welche Länder "Hubs" sind lässt sich zum Beispiel das Attribut `from_type` als Kantenfarbe darstellen.

```{r message=FALSE, warning=FALSE, error=FALSE}

football_graph = graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)

V(football_graph)[V(football_graph)$type == "other"]$shape <- 21
V(football_graph)[V(football_graph)$type == "hub"]$shape <- 22
V(football_graph)[V(football_graph)$type == "auth"]$shape <- 23
V(football_graph)[V(football_graph)$type == "hubauth"]$shape <- 24

plt <- ggraph(football_graph, layout = 'linear', circular = TRUE) +
      # Kanten
      geom_edge_arc(aes(alpha=weight,color=from_type,width=weight,start_cap = label_rect(node1.name), end_cap = label_rect(node2.name)), 
      arrow = arrow(type = "closed", length = unit(2, 'mm'))) +
      scale_edge_width(range = c(0.2, 2.0)) +
      # Knoten
      geom_node_point(aes(size=20,color=factor(type),shape=factor(shape))) +
      scale_size(range = c(2,10)) +
      coord_equal()+
      my_theme()
      
# Knoten-Labels
plt +
  geom_node_label(aes(label = name),family="serif", alpha=0.9,repel="False",nudge_x = plt$data$x * .15, nudge_y = plt$data$y * .15)

```

Alternativ kann anstelle von `geom_edge_arc` die Funktion `geom_edge_fan2` benutzt werden. Diese greift im nächsten Beispiel auf das `type` Attribute der Knoten zu und erzeugt einen Farbeverlauf basierend auf den jeweiligen Attributwerten von Start- und Zielknoten.

```{r message=FALSE, warning=FALSE, error=FALSE}

football_graph = graph_from_data_frame(d=edges,directed=TRUE,vertices=nodes)

V(football_graph)[V(football_graph)$type == "other"]$shape <- 21
V(football_graph)[V(football_graph)$type == "hub"]$shape <- 22
V(football_graph)[V(football_graph)$type == "auth"]$shape <- 23
V(football_graph)[V(football_graph)$type == "hubauth"]$shape <- 24

plt <- ggraph(football_graph, layout = 'linear', circular = TRUE) +
      # Kanten
      geom_edge_fan2(aes(colour=node.type,width=weight,alpha=weight)) +
      scale_edge_width(range = c(0.2, 2.0)) +
      # Knoten
      geom_node_point(aes(size=20,color=factor(type),shape=factor(shape))) +
      scale_size(range = c(2,10)) +
      coord_equal()+
      my_theme()
      
# Knotenlabels
plt +
  geom_node_label(aes(label = name),family="serif", alpha=0.9,repel="False",nudge_x = plt$data$x * .15, nudge_y = plt$data$y * .15)
```

## Literatur:

<https://igraph.org>

<https://kateto.net/networks-r-igraph>

<https://www.r-graph-gallery.com/248-igraph-plotting-parameters.html>

<https://kmlawson.github.io/dh-tutorials/network.html>

<https://mr.schochastics.net/material/netvizr/>

<http://vlado.fmf.uni-lj.si/pub/networks/data/sport/football.htm>

<https://www.researchgate.net/figure/Visualization-of-the-Hubs-and-authorities-partitioning-of-the-transatlantic-network_fig9_265157059>
