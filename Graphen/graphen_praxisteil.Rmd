---
title: 'Graphenvisualisierung igraph und ggraph: Praxis'
author: "Dr. Tobias Englmeier"
date: "`r Sys.Date()`"
output: 
  html_document: 
    toc: yes
editor_options: 
  chunk_output_type: console
---

## Visualisierung von Verbindungen bekannter deutscher Lyriker

Die bereitgestellten Datensätze zu "gedichte_nodes.csv" und "gedichte_edges.csv" enthalten Informationen zu ca. 50 bekannten Dichtern aus dem deutschsprachigen Raum. Die Verknüpfung der Dichter untereinander beruht auf gemeinsam vorkommenden Phrasen, die diese benutzt haben[^1].

[^1]: Der vorliegende Datensatz stellt einen Auszug der in [Using an Advanced Text Index Structure for Corpus Exploration in Digital Humanities](https://search.proquest.com/openview/d7126f017a63751264677a3ac7848a47/1?pq-origsite=gscholar&cbl=5124193) verwendeten Daten dar und wurde von Dr. Tobias Englmeier, Dr. Stefan Gerdjikov und Prof. Dr. Klaus U. Schulz erstellt.

Ihre Aufgabe ist es, diese Daten einzulesen und sie mit Hilfe von `ggraph` und `igraph` eine Netzwerkvisualisierung dieser Daten zu erstellen.

-   Laden Sie zuerst die Pakete: `igraph`, `ggraph`, `graphlayouts`, `tidygraph` und `readr`

```{r}
```

-   Testen Sie `igraph` indem Sie die Funktion `make_graph("Diamond")` aufrufen und geben Sie das Ergbnis mit `plot()` aus.

```{r}
```

-   Laden Sie die Dateien "gedichte_nodes.csv" und "gedichte_edges.csv" und speichern Sie die Ergebnisse in entprechenden Variablen.

```{r}
```

-   Inspizieren Sie die eingelesenen Daten und verschaffen Sie sich ein Überblick über diese.

```{r}
```

-   Erzeugen Sie nun ein Graphen-Objekt und geben Sie es mit `ggraph` aus (Tipp: Nutzen Sie `graph_from_data_frame()` zur Erstellung und `geom_node_point()` sowie `geom_edge_link()` , um Knoten und Kanten zu generieren.

```{r}
```

-   Wählen Sie das Layout "kk" und geben Sie den Graphen mit diesem aus. Tipp: Setzen Sie den Zusatzparameter `maxiter` auf einen höheren Wert (etwa 400), damit die Knoten genug Abstand zu einander haben.

```{r}
```

-   Verwenden Sie `geom_node_label`, um ihrem Graphen mit Knoten-Labels zu versehen, die den Namen des jeweiligen Dichters enthalten. Tipp: Damit die Labels nicht unterhalb der Kanten gezeichnet werden, müssen Sie die Aufrufreihenfolge der Funktionen anpassen.

```{r}
```

-   Optional: Erstellen Sie ein eigenes Theme und fügen Sie es Ihrer Visualisierung hinzu

```{r}
```

```{r}
```

-   Setzen Sie den alpha-Wert der Kanten nun auf das Kantenattribut "weight" und den Farbwert der Knoten auf "cent".

```{r}
```

-   Sehen Sie sich die `paste` - Funktion in der R-Dokumentation an und fügen Sie das Attribut "name" zusammen mit "year" (getrennt mit einem Leerzeichen) dem Knotenlabel hinzu. Das "year" - Attribut kann außerdem noch Klammern umgeben werden.

```{r}
```

### Interessante Beobachtungen und Fragen:

-   Was könnte der Grund sein, dass Fritz Reuter nur mit John Brinckman in Verbindung steht ?

-   Warum stehen Arno Holz, Wilhelm Arent und Hermann Conradi in Verbindung ?

-   Eine dichtere Stelle im Netzwerk beinhaltet viele Dichter aus der Epoche der Romantik. Welcher Dichter passt nicht in diese Zeit, ist aber trotzdem eng mit Dichtern aus der Romantik verknüpft ? Was könnte ein möglicher Grund dafür sein ?

-   Welcher Dichter scheint für die Romantik am "einflussreichsten zu sein", bzw. besitzt die meisten Übereinstimmungen mit anderen Lyrikern ?
