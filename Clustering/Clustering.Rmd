---
title: "Techniken der Informationsvisualisierung II: Clustervisualisierungen"
output:
  html_notebook:
#    code_folding: hide
---


#### Axel Wisiorek (LMU München)
#### 21.3.2022


### Pakete laden:

```{r}
library(ggplot2) # ggplot2-Basispaket
library(GGally) # Erweiterung für ggplot2
library(rgl) # Paket für 3D-Visualisierungen

library(scales) # ggplot default colors (für hue_pal)
library(RColorBrewer) # Paket für Farbpaletten
```



## 1. Gruppierte Daten: Visualisierungsmethoden

### 1.1 Übersicht verwendeter Datensatz

#### *Stilistik-Daten von Texten (Brown-Korpus)*

https://en.wikipedia.org/wiki/Brown_Corpus#Sample_distribution

##### Quantitative linguistische Merkmale:
  - **Durchschnittliche Satzlänge** (`av_sent_length`)
  - **Durchschnittliche Wortlänge** (`av_word_length`)
  - **Type-Token-Relation** (`lex_diversity`)


##### Klassen/Gruppen/Kategorien (`genre`):
  - ***government*** 
  - ***romance***


```{r}
data <- read.csv("stylo_data.csv")
```

```{r}
head(data)
```

Der Datensatz enthält neben metrischen Feature-Variablen also auch eine **kategoriale Variable** mit den ***Text-Klassen-Labels***, die im folgenden zur gruppierenden Darstellung verwendet wird.

Hier eine zusammenfassende Statistik des Datensatzes:

```{r}
summary(data)
```



### 1.2 Gruppierte Scatterplots

Gruppierte Scatterplots stellen die Wertverteilung für jeweils zwei Variablen ***farbkodiert*** nach Klassenzugehörigkeit dar, hier gruppiert nach Textsorten.

Neben diesen **paarweisen Streudiagrammen** können mit der `ggpairs()`-Funktion des `GGally`-Erweiterungspakets für `ggplot2`in einer Übersichtsgraphik auch **klassenspezifische Boxplots**, **Korrelationsplots** und **Dichtegraphen** ausgegeben werden:

- *Library: `GGally` - https://www.rdocumentation.org/packages/GGally/versions/1.5.0*
- *Funktion: `ggpairs()` - https://www.rdocumentation.org/packages/GGally/versions/1.5.0/topics/ggpairs*



```{r, fig.height=3}
ggpairs(data, 
        aes(color = genre), # nach genre-Variable gruppieren, Verwendung Default ggplot-colors
        columns = 1:4, # verwendete Variablen (3 metrische und 1 kategoriale für Gruppierung)
        progress = FALSE #disable progress output
      )
```




### 1.3 Parallelkoordinaten-Plot

Ein **Parallelkoordinatenplot** bildet die Werte der verschiedenen Variablen in einem  Datensatz  als verbundene Linien in einem **Koordinatensystem mit parallel angeordnete Achsen** ab; die Gruppenzugehörigkeit wird farbkodiert. Dazu müssen die Variablen skaliert werden (es stehen verschiedene Skalierungsmethoden zur Verfügung).

- *Library: `GGally` - https://www.rdocumentation.org/packages/GGally/versions/1.5.0*
- *Funktion: `ggparcoord()` - https://www.rdocumentation.org/packages/GGally/versions/1.5.0/topics/ggparcoord*


```{r, fig.height=3}
ggparcoord(
  data,
  columns = 1:3,
  groupColumn = 4,
  scale = "std" # "std": univariately, subtract mean and divide by standard deviation
  )
```








### 1.4 3D-Scatterplot

Mit dem `RGL`-Paket können interaktive 3D-Visualisierungen realisiert werden. Es basiert auf OpenGL  (in R Studio) bzw. WebGL (in HTML-Dokumenten im Browser bzw. R-Markdown-Dokumenten in R Studio) als Grafikschnittstelle, s. https://www.r-graph-gallery.com/interactive-charts.html#3dscatter für ein Beispiel.



- *rgl Overview: https://cran.r-project.org/web/packages/rgl/vignettes/rgl.html*
- *User Interaction in WebGL: https://cran.r-project.org/web/packages/rgl/vignettes/WebGL.html*


**Beachten Sie**: Nicht alle möglichen 3D-Visualisierungen sind auch sinnvoll, siehe https://www.data-to-viz.com/caveat/3d.html. Z.B. machen 3D-Scatterplots nur als interaktiver Plot Sinn.

- *Library: `rgl` - https://www.rdocumentation.org/packages/rgl/versions/0.108.3, https://dmurdoch.github.io/rgl/*
- *Funktion: `plot3d()` - https://rdrr.io/cran/rgl/man/plot3d.html, https://cran.r-project.org/web/packages/rgl/vignettes/rgl.html#basics-and-high-level-functions, https://dmurdoch.github.io/rgl/dev/reference/plot3d.html*


```{r results='hide'}
# Output rgl-Plot in R-Markdown Dokument:
setupKnitr()
```

```{r results='hide'}
open3d() # open in new window
```

```{r}
# 3D-Scatterplot mit RGL:
plot3d( 
  x=data$`av_sent_length`, y=data$`av_word_length`, z=data$`lex_diversity`,
  col = hue_pal()(2)[(data$genre)], # ggplot default colors explizit machen und verwenden
  type = 's', 
  radius = .4,
  xlab="Average Sentence Length", ylab="Average Word Length", zlab="Lexical Diversity")
```

```{r results='hide'}
rglwidget() # display in R-markdown document
```





### 1.5 Gelabelter Scatterplot

Zusätzlich (oder alternativ) zu einer Farbkodierung der Klassenzugehörigkeit in einem Scatterplot (`geom_point()`) können in `ggplot2` mit `geom_text()` die Datenpunkte auch **gelabelt** werden, etwa, indem die Label der entsprechenden kategorialen Variable verwendet werden.

- *Funktion: `geom_point()` - https://ggplot2.tidyverse.org/reference/geom_point.html*
- *Funktion: `geom_text()` - https://ggplot2.tidyverse.org/reference/geom_text.html*


```{r}
data$genre
```


```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = genre), size = 10)+
# Gruppen-Label hinzufügen:
  geom_text(label=data$genre,x = data$av_sent_length, y = data$av_word_length, check_overlap = T)+
  labs(x = 'Average Sentence Length', y = 'Average Word Length', color='Text Categories')+
  theme_bw()
```










## 2. Clustering: Methodik und Visualisierungen

### 2.1 Clustering als Methode der explorativen Datenanalyse

Clustering ist eine Methode der explorativen Datenanalyse (***unsupervised classification***).

**Clustering-Algorithmen** finden in den Daten Gruppierungen (Klassen) von ähnlichen Objekten (Mengen im Merkmalsraum nahe beieinanderliegender Datenpunkte). Im Gegensatz zur Klassifikation (*supervised classification*) ist hier die Klasseneinteilung nicht vorgegeben.

Die in den Daten gefundenen Clustergruppierungen können anschließend evaluiert werden, u.a. bzgl. ihrer Übereinstimmung mit apriori gegebenen Datengruppierungen (**externe Clusterevaluation**).

Der im folgenden verwendete **k-means-Algorithmus** ist ein einfacher Clustering-Alogrithmus, der nach Vorgabe einer Clusteranzahl eine Einteilung des Merkmalraums vornimmt, so dass die Summe der quadrierten Abweichungen von der Schwerpunkten der Cluster (ihrer Mittelwerte = *Means*) minimal ist (Minimierung der Summe der Varianzen der Cluster). Jedes Objekt wird dadurch dem nächstgelegenen Clusterschwerpunkt zugeordnet.


![K-Means-Algorithmus: https://de.wikipedia.org/wiki/K-Means-Algorithmus#Lloyd-Algorithmus](https://upload.wikimedia.org/wikipedia/commons/e/ea/K-means_convergence.gif)


### 2.2 Bestimmung der optimalen Clusteranzahl

Zur Feststellung der optimalen Clusteranzahl (basierend auf der in den Daten vorhandenen Strukturierung) kann die sog. Elbow-Methode angewendet werden, die das Mittel der Fehlerquadrate für **verschiedene Clusteranzahlen** berechnet (**WCSS = *within-cluster sum of square***). 

(Die `kmeans()`-Implementierung in R ist Teil des mitgelieferten `stats`-Pakets: https://www.rdocumentation.org/packages/stats/versions/3.6.2/topics/kmeans.)

Anhand des Plots kann der **'Knick'** (*Elbow*) in der entsprechenden Kurve gefunden werden, nach dem die WCSS nur noch wenig sinkt (nahezu parallel zur x-Achse verläuft); ab hier nimmt die Varianz innerhalb der Cluster durch feinere Clustereinteilungen (höhere Clusteranzahl) nicht mehr bedeutend ab.

- *Funktion: `geom_line()` - https://plotly.com/ggplot2/geom_line*
- *Funktion: `geom_point()` - https://ggplot2.tidyverse.org/reference/geom_point.html*



```{r}
set.seed(123) # random seed für reproduzierbares Ergebnis (Cluster-Zentren werden zufällig initialisiert) 
wcss <- vector()
for (i in 1:10) wcss[i] <- sum(kmeans(data[, -4], i)$withinss)
df <- data.frame(x=c(1:10), wcss)
```

```{r, fig.height=3}
ggplot(df, aes(x = x, y = wcss))+
geom_line()+
geom_point()+
scale_x_continuous(breaks = c(1:10))+
labs(x = 'Cluster-Anzahl', y = 'WCSS')+
ggtitle("Elbow-Methode")+
theme_bw()
```







### 2.3 k-means-Clustering mit 3 Clustern

Entsprechend des *Elbow* bei 3 Clustern wird zunächst diese **Clusteranzahl ausgewählt (k = 3)**, das **k-means-Clustering** dafür durchgeführt und die Cluster-Gruppierung (Cluster 1, 2, 3) als Faktor in dem Dataframe des Datensatzes abgespeichert (`cluster.kmean.3`). Mit `table()` können die Frequenzverteilung der Datensätze auf die Clustergruppen berechnet werden:

```{r}
set.seed(123) # für reproduzierbares Ergebnis
# Clustering:
km <- kmeans(x = data[, -4] , centers = 3)
data$cluster.kmean.3 <- as.factor(km$cluster)
table(data$cluster.kmean.3)
```


```{r}
head(data)
```

Anschließend werden wieder **gruppierte Scatterplots** ausgegeben, diesmal aber mit der in den Daten gefundenen **Cluster-Gruppierung**. Dafür wird eine neue Farbpalette verwendet:


```{r, fig.height=3}
#brewer.pal(n = 3, name = 'Paired')
display.brewer.pal(n = 3, name = 'Paired')
```



```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = cluster.kmean.3) , size = 10)+ 
# Farbpalette festlegen:
  scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
  labs(x = 'Average Sentence Length', y = 'Average Word Length', color='k-means Cluster')+
  theme_bw()
```

```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = lex_diversity, 
                 color = cluster.kmean.3) , size = 10)+ 
  labs(x = 'Average Sentence Length', y = 'Lexical Diversity', color='k-means Cluster')+
# Farbpalette festlegen:
  scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
  theme_bw()
```


Zur **Clusterevaluation** wird die im k-means-Clustering gefundene Gruppierung mit der apriori gegebenen Klassifizierung (hier: die beiden Textsorten) verglichen:

```{r}
cm <- table(data$genre, data$cluster.kmean.3)
cm
```

```{r, fig.height=2}
plot(cm, type = "h", main="Genres vs. Clustering")
```








### 2.4 k-means mit 2 Clustern

Zum direkten Vergleich der Übereinstimmung mit der apriori gegebenen Klassifizierung (hier: die beiden Textsorten) werden im Folgenden **zwei Cluster-Schwerpunkte** berechnet (k = 2)  und die Cluster-Gruppierung (Cluster 1, 2) wieder als Faktor in dem Dataframe des Datensatzes abgespeichert (`cluster.kmean.2`):


```{r}
set.seed(123) # für reproduzierbares Ergebnis
# Clustering:
km <- kmeans(x = data[, -4] , centers = 2)
data$cluster.kmean.2 <- as.factor(km$cluster)
table(data$cluster.kmean.2)
```



```{r}
head(data)
```

Anschließend werden wieder **Cluster-gruppierte Scatterplots** ausgegeben:

```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = cluster.kmean.2) , size = 10)+ 
# Farbpalette festlegen:
  scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
  labs(x = 'Average Sentence Length', y = 'Average Word Length', color='k-means Cluster')+
  theme_bw()
```


```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = lex_diversity, 
                 color = cluster.kmean.2) , size = 10)+ 
  labs(x = 'Average Sentence Length', y = 'Lexical Diversity', color='k-means Cluster')+
# Farbpalette festlegen:
  scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
  theme_bw()
```



Zur **Clusterevaluation** wird die im k-means-Clustering gefundene Gruppierung mit der apriori gegebenen Klassifizierung (hier: die beiden Textsorten) verglichen:

```{r}
cm <- table(data$genre, data$cluster.kmean.2)
cm
```

```{r, fig.height=2}
plot(cm, type = "h", main="Genres vs. Clustering")
```











## 3. Clustering mit Feature-Scaling


### 3.1 Motivation für Feature-Scaling

Unterschiedliche Skalen führen zu einem **inhomogenen Merkmalsraum**. Dies führt auch zu Verzerrungen in der Distanzberechung beim Clustering. 

Z.B. ist im Datensatz der Wertebereich des Features 'Satzlänge' von 10-35, für 'Wortlänge' von 3-5. Dies kann mit folgenden **Multi-Panel Plots** der ersten beiden Features verdeutlicht werden (im zweiten Fall sind die Achsen (!) auf einheitliche Länge skaliert):

- *Funktion: `facet_wrap()` - https://ggplot2.tidyverse.org/reference/facet_wrap.html*

```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
 geom_point(aes(x = av_sent_length, y = av_word_length), stroke = 2)+
# Panels nach Genre-Gruppen erstellen:
 facet_wrap(~ genre)+ 
 labs(x = 'Average Sentence Length', y = 'Average Word Length')+
 theme_bw()
```


```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
 geom_point(aes(x = av_sent_length, y = av_word_length), stroke = 2)+
 coord_cartesian(xlim=c(10,35), ylim=c(0,25)) +
# Panels nach Genre-Gruppen erstellen:
 facet_wrap(~ genre)+ 
 labs(x = 'Average Sentence Length', y = 'Average Word Length')+
 theme_bw()
```





### 3.2 Skalierung des Feature-Sets

Durch **Skalierung** der Feature kann der Merkmalsraum **homogenisiert** werden. 

Der Datensatz wird dazu zunächst erneut eingelesen und anschließend skaliert:

```{r}
data_scaled <- read.csv("stylo_data.csv")
head(data_scaled)
```


Die `scale()`-Funktion des `stats`-Pakets von R führt eine sog. **z-Standardisierung** aus, wobei jede Feature-Dimension auf den Mittelwert der Feature-Werte zentriert (0) und durch deren Varianz normiert wird. Es entsteht so ein homogener Feature-Space mit vergleichbaren Dimensionen.


```{r}
# Skalierung der Variablen:
data_scaled[c(1,2,3)] <- lapply(data[c(1,2,3)], function(x) c(scale(x)))
head(data_scaled)
```

Nach der Skalierung zeigt sich im **Multi-Panel Plot** der ersten beiden Features, dass die Skalen der verschiedenen Variablen nun vergleichbar sind:


```{r, fig.height=3}
ggplot(data_scaled)+
#Scatterplot erstellen:
 geom_point(aes(x = av_sent_length, y = av_word_length), stroke = 2)+
# Panels nach Genre-Gruppen erstellen:
 facet_wrap(~ genre)+ 
 labs(x = 'Average Sentence Length', y = 'Average Word Length')+
 theme_bw()
```





### 3.3 Bestimmung der optimalen Clusteranzahl

Zur Feststellung der optimalen Clusteranzahl für den skalierten Datensatz kann wieder die **Elbow-Methode** angewendet werden:

```{r}
set.seed(123) # für reproduzierbares Ergebnis
wcss <- vector()
for (i in 1:10) wcss[i] <- sum(kmeans(data_scaled[, -4], i)$withinss)
df <- data.frame(x=c(1:10), wcss)
```


```{r, fig.height=3}
ggplot(df, aes(x = x, y = wcss))+
geom_line()+
geom_point()+
scale_x_continuous(breaks = c(1:10))+
labs(x = 'Cluster-Anzahl', y = 'WCSS')+
ggtitle("Elbow-Methode")+
theme_bw()
```



### 3.4 k-means mit 2 Clustern (skaliert)

Entsprechend des *Elbow* bei 2 Clustern wird nun für den skalierten Datensatz diese **Clusteranzahl ausgewählt (k = 2)**, das **k-means-Clustering** dafür durchgeführt und die Cluster-Gruppierung (Cluster 1, 2) als Faktor in dem Dataframe des Datensatzes abgespeichert (`cluster.kmean.2`). 

Mit `table()` können wieder die Frequenzverteilung der Datensätze auf die Clustergruppen berechnet werden.


```{r}
set.seed(123) # für reproduzierbares Ergebnis
# Clustering:
km <- kmeans(x = data_scaled[, -4] , centers = 2)
data_scaled$cluster.kmean.2 <- as.factor(km$cluster)
table(data_scaled$cluster.kmean.2)
```



```{r}
head(data_scaled)
```

Anschließend wird der **Cluster-gruppierte Scatterplot** der ersten beiden Features ausgegeben, jetzt auf den skalierten Daten:

```{r, fig.height=3}
ggplot(data_scaled)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = cluster.kmean.2) , size = 10)+ 
# Farbpalette festlegen:
    scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
    labs(x = 'Average Sentence Length', y = 'Average Word Length', color='k-means Cluster')+
  theme_bw()
```

Zum Vergleich der gelabelte Scatterplot der ersten beiden Features von oben mit den **Apriori-Textsorten**:

```{r, fig.height=3}
ggplot(data)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = genre), size = 10)+
# Gruppen-Label hinzufügen:
  geom_text(label=data$genre,x = data$av_sent_length, y = data$av_word_length, check_overlap = T)+
  labs(x = 'Average Sentence Length', y = 'Average Word Length', color='Text Categories')+
  theme_bw()
```

Abschließend noch ein kombinierter Plot mit **farbkodierten Clustergruppen** und **gelabelten Textsorten**:

```{r, fig.height=3}
ggplot(data_scaled)+
# Scatterplot erstellen:
  geom_point(aes(x = av_sent_length, y = av_word_length, 
                 color = cluster.kmean.2) , size = 10)+ 
# Farbpalette festlegen:
    scale_color_manual(values = brewer.pal(n = 3, name = 'Paired'))+
# Gruppen-Label hinzufügen:
  geom_text(label=data_scaled$genre,x = data_scaled$av_sent_length, y = data_scaled$av_word_length, check_overlap = T)+
  labs(x = 'Average Sentence Length', y = 'Average Word Length', color='k-means Cluster')+
  theme_bw()
```






Zur **Clusterevaluation** wird die im k-means-Clustering gefundene Gruppierung mit der apriori gegebenen Klassifizierung (hier: die beiden Textsorten) verglichen. Es zeigt sich, dass nach der Skalierung die anhand der 3  Stilistik-Features des Datensatzes berechnetet Clustergruppierungen nahezu vollständig mit den Textsorten übereinstimmen:

```{r}
cm <- table(data_scaled$genre, data_scaled$cluster.kmean)
cm
```

```{r, fig.height=2}
plot(cm, type = "h", main="Genres vs. Clustering")
```


